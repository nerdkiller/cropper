use std::env;
use std::process::exit;
use std::path::Path;
use std::ffi::OsStr;
use std::time::{SystemTime, UNIX_EPOCH};
use std::collections::HashMap;
use image::GenericImageView;
use rand::seq::SliceRandom;

#[macro_export]
macro_rules! s {
    ($s: expr) => {
        $s.to_string()
    };

    () => {
        String::new()
    };
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let sides: Vec<String> = args[1].split(",").map(|s| s!(s)).collect();

    let threshold = args[2].parse::<f32>().unwrap_or_else(|_| {
        println!("Wrong threshold.");
        exit(0);
    });

    let border = args[3].parse::<u32>().unwrap_or_else(|_| {
        println!("Wrong border.");
        exit(0);
    });

    let spath = args
        .iter()
        .skip(4)
        .map(|s| s.clone())
        .collect::<Vec<String>>()
        .join(" ");

    if spath == String::from("") {
        exit(0)
    }

    let path = Path::new(&spath);
    
    let extension = match path.extension().and_then(OsStr::to_str) {
        Some(ext) => ext,
        None => {
            println!("Invalid file extension.");
            exit(0);
        }
    };

    let mut img = match image::open(&spath) {
        Ok(im) => im,
        Err(_) => {
            println!("Invalid file path.");
            exit(0);
        }
    };

    let dims = img.dimensions();
    let bytes: Vec<u8> = img.to_rgb().to_vec();
    let mut res = HashMap::new();
    
    let all_sides = [s!("top"), s!("bottom"), s!("left"), s!("right")];

    for side in all_sides.iter() {
        if !sides.contains(side) {
            res.insert(side, 0);
            continue;
        }

        let mut ans = check_region(&bytes, dims.0, dims.1, &side, threshold);
        
        ans = if border >= ans {
            0
        } else {
            ans - border
        };

        res.insert(side, ans);
    }

    let top = *res.get(&s!("top")).unwrap();
    let bottom = *res.get(&s!("bottom")).unwrap();
    let left = *res.get(&s!("left")).unwrap();
    let right = *res.get(&s!("right")).unwrap();
    
    img = img.crop(left, top, dims.0 - left - right, dims.1 - top - bottom);

    let new_path = format!("cropped/{}_{}.{}", random_word(), now(), extension);
    img.save(&new_path).unwrap();
    println!("{}", &new_path);
}

fn check_region(bytes: &Vec<u8>, width: u32, height: u32, side: &str, threshold: f32) -> u32 {
    let mut n = 0;
    let mut levels = 0;
    let mut lab_set = false;
    let mut lab1 = lab::Lab::from_rgb(&[0, 0, 0]);

    let mut i = match side {
        "top" | "left" => 0,
        "bottom" => bytes.len() - 1,
        "right" => (width as usize * 3) - 1,
        _ => exit(0)
    };

    for _ in 0..bytes.len() {
        let b1;
        let b2;
        let b3;

        match side {
            "top" | "left" => {
                b1 = bytes[i];
                b2 = bytes[i + 1];
                b3 = bytes[i + 2];
            },
            "bottom" | "right" => {
                b3 = bytes[i];
                b2 = bytes[i - 1];
                b1 = bytes[i - 2];
            },
            _ => {exit(0)}
        }

        if !lab_set {
            lab1 = lab::Lab::from_rgb(&[b1, b2, b3]);
            lab_set = true;
        } else {
            let lab2 = lab::Lab::from_rgb(&[b1, b2, b3]);
            let distance = lab1.squared_distance(&lab2);
            
            if distance >= threshold {
                break;
            }
        }

        n += 1;

        let row_done = match side {
            "top" | "bottom" => n == width,
            "left" | "right" => n == height,
            _ => exit(0)
        };
        
        if row_done {
            levels += 1;
            n = 0;
        }

        i = match side {
            "top" => i + 3,
            "bottom" => i - 3,
            "left" => { 
                if row_done {
                    levels * 3
                } else {
                    ((width as usize * 3) * n as usize) + (levels * 3)
                }
            },
            "right" => {
                if row_done {
                    ((width as usize * 3) as usize) - (levels * 3) - 1
                } else {
                    ((width as usize * 3) * n as usize) - (levels * 3) - 1
                }
            },
            _ => exit(0)
        };

        match side {
            "top" | "left" => {
                if i >= bytes.len() - 3 {
                    break;
                }
            },
            "bottom" | "right" => {
                if i < 3 {
                    break;
                }
            },
            _ => {exit(0)}
        }
    }

    let ans = match side {
        "top" | "bottom" => levels,
        "left" | "right" => levels,
        _ => exit(0)
    };

    let max = 0.4;

    match side {
        "top" | "left" => {
            if ans > (max * height as f32) as usize {
                return 0;
            }
        },
        "bottom" | "right" => {
            if ans > (max * width as f32) as usize {
                return 0;
            }
        },
        _ => exit(0)
    };

    return ans as u32;
}

fn now() -> u128 {
    let start = SystemTime::now();
    let since_the_epoch = start.duration_since(UNIX_EPOCH).unwrap();
    since_the_epoch.as_millis()
}

fn random_word() -> String {
    let a = vec!["a","e","i","o","u"];  
    let b = vec!["b", "c", "d", "f", "g", "h", "j", "k", "l", "m", 
    "n", "p", "r", "s", "t", "v", "w", "x", "y", "z"];
    let mut word = "".to_string();

    for i in 1..=6 {
        let letter = if i % 2 == 0 {
            b.choose(&mut rand::thread_rng())
        } else {
            a.choose(&mut rand::thread_rng())
        };

        word.push_str(letter.unwrap());
    }

    return word;
}